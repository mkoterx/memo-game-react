import { shuffle } from "./util";
import { Categories, ItemType } from "./enums";

const itemsPokemons = [
  { val: fullSrc(Categories.pokemons, "abra", "svg"), label: "abra" },
  { val: fullSrc(Categories.pokemons, "backpack", "svg"), label: "backpack" },
  {
    val: fullSrc(Categories.pokemons, "bellsprout", "svg"),
    label: "bellsprout"
  },
  {
    val: fullSrc(Categories.pokemons, "bullbasaur", "svg"),
    label: "bullbasaur"
  },
  {
    val: fullSrc(Categories.pokemons, "charmander", "svg"),
    label: "charmander"
  },
  { val: fullSrc(Categories.pokemons, "dratini", "svg"), label: "dratini" },
  { val: fullSrc(Categories.pokemons, "eevee", "svg"), label: "eevee" },
  {
    val: fullSrc(Categories.pokemons, "jigglypuff", "svg"),
    label: "jigglypuff"
  },
  { val: fullSrc(Categories.pokemons, "mankey", "svg"), label: "mankey" },
  { val: fullSrc(Categories.pokemons, "mega-ball", "svg"), label: "mega ball" },
  { val: fullSrc(Categories.pokemons, "meowth", "svg"), label: "meowth" },
  { val: fullSrc(Categories.pokemons, "mew", "svg"), label: "mew" },
  { val: fullSrc(Categories.pokemons, "pidgey", "svg"), label: "pidgey" },
  { val: fullSrc(Categories.pokemons, "pikachu", "svg"), label: "pikachu" },
  {
    val: fullSrc(Categories.pokemons, "pikachu_jupiter", "svg"),
    label: "pikachu"
  },
  {
    val: fullSrc(Categories.pokemons, "pikachu_orange", "svg"),
    label: "pikachu"
  },
  { val: fullSrc(Categories.pokemons, "player1", "svg"), label: "player 1" },
  { val: fullSrc(Categories.pokemons, "player2", "svg"), label: "player 2" },
  { val: fullSrc(Categories.pokemons, "pokeball", "svg"), label: "pokeball" },
  { val: fullSrc(Categories.pokemons, "pokeballs", "svg"), label: "pokeballs" },
  { val: fullSrc(Categories.pokemons, "pokecoin", "svg"), label: "pokecoin" },
  { val: fullSrc(Categories.pokemons, "psyduck", "svg"), label: "psyduck" },
  { val: fullSrc(Categories.pokemons, "rattata", "svg"), label: "rattata" },
  {
    val: fullSrc(Categories.pokemons, "smartphone", "svg"),
    label: "smartphone"
  },
  { val: fullSrc(Categories.pokemons, "snorlax", "svg"), label: "snorlax" },
  { val: fullSrc(Categories.pokemons, "squirtle", "svg"), label: "squirtle" },
  { val: fullSrc(Categories.pokemons, "star", "svg"), label: "star" },
  { val: fullSrc(Categories.pokemons, "superball", "svg"), label: "superball" },
  {
    val: fullSrc(Categories.pokemons, "ultra-ball", "svg"),
    label: "ultra ball"
  },
  { val: fullSrc(Categories.pokemons, "venonat", "svg"), label: "venonat" },
  { val: fullSrc(Categories.pokemons, "weedle", "svg"), label: "weedle" },
  { val: fullSrc(Categories.pokemons, "zubat", "svg"), label: "zubat" }
];

const itemsGames = [
  { val: fullSrc(Categories.games, "word-of-wardcraft", "png") },
  { val: fullSrc(Categories.games, "gta_sa", "png") },
  { val: fullSrc(Categories.games, "lol", "png") },
  { val: fullSrc(Categories.games, "cs_1_6", "png") },
  { val: fullSrc(Categories.games, "cod", "png") },
  { val: fullSrc(Categories.games, "battlefield_3", "png") },
  { val: fullSrc(Categories.games, "assasins_creed", "png") },
  { val: fullSrc(Categories.games, "nfs_carbon", "png") }
];

const itemsPowerPuffGirls = [
  {
    val: fullSrc(Categories.powerPuffGirls, "blossom", "png"),
    label: "blossom"
  },
  {
    val: fullSrc(Categories.powerPuffGirls, "bubbles", "png"),
    label: "bubbles"
  },
  {
    val: fullSrc(Categories.powerPuffGirls, "buttercup", "png"),
    label: "buttercup"
  },
  {
    val: fullSrc(Categories.powerPuffGirls, "him", "png"),
    label: "HIM"
  },
  {
    val: fullSrc(Categories.powerPuffGirls, "mayor_of_townsville", "png"),
    label: "Mayor of Townsville"
  },
  {
    val: fullSrc(Categories.powerPuffGirls, "miss_bellum", "png"),
    label: "Miss Bellum"
  },
  {
    val: fullSrc(Categories.powerPuffGirls, "mojo_jojo", "png"),
    label: "Mojo Jojo"
  },
  {
    val: fullSrc(Categories.powerPuffGirls, "prof_utonium", "png"),
    label: "Prof Utonium"
  }
];

const data = {
  [Categories.letters]: {
    type: ItemType.text,
    items: [
      "A",
      "B",
      "C",
      "D",
      "E",
      "F",
      "G",
      "H",
      "I",
      "J",
      "K",
      "L",
      "M",
      "N",
      "O",
      "P",
      "Q",
      "R",
      "S",
      "T",
      "U",
      "V",
      "W",
      "X",
      "Y",
      "Z"
    ]
  },
  [Categories.cities]: {
    type: ItemType.text,
    items: [
      "Skopje",
      "Pristina",
      "Belgrade",
      "Sofia",
      "Teirana",
      "Athens",
      "Istanbul",
      "Ankara",
      "Sarajevo",
      "Zagreb",
      "Ljubljana",
      "Budapes",
      "Bratislava",
      "Viena",
      "Berlin",
      "Munich",
      "Frankfurt",
      "Stutgard",
      "Hamburg",
      "Amsterdam",
      "Roterdam",
      "Paris",
      "Nice",
      "Rome",
      "Milano",
      "Trieste",
      "Barcelona",
      "Madrid",
      "Seville",
      "Valencia",
      "Lisbon",
      "Porto",
      "Faro",
      "Lagos",
      "London",
      "Manchester",
      "New York",
      "Los Angeles",
      "Las Vegas",
      "Detroid",
      "Chicago",
      "Doublin"
    ]
  },
  [Categories.pokemons]: {
    type: ItemType.image,
    items: itemsPokemons
  },
  [Categories.games]: {
    type: ItemType.image,
    items: itemsGames
  },
  [Categories.powerPuffGirls]: {
    type: ItemType.image,
    items: itemsPowerPuffGirls
  }
};

export function getMatchables(category, count) {
  const { type, items } = data[category];
  return { type, items: shuffle(items).slice(0, count) };
}

function fullSrc(category, imgName, extension) {
  return `./resources/categories/${category}/${imgName}.${extension}`;
}
