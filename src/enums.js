export const GameStatus = Object.freeze({
  idle: 0,
  inProgress: 1,
  win: 2,
  loose: 3
});

export const Categories = Object.freeze({
  pokemons: "pokemons",
  games: "games",
  powerPuffGirls: "powerPuffGirls",
  cities: "cities",
  letters: "letters"
});

export const ItemType = Object.freeze({ text: 1, image: 2 });
