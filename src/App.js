import React from "react";
import "./App.css";
import { getMatchables } from "./data";
import { shuffle, getObjectPropsAsSeq } from "./util";
import { GameStatus, Categories, ItemType } from "./enums";
import Labels from "./labels.json";

class Square extends React.Component {
  shouldComponentUpdate(nextProps) {
    return this.props.item !== nextProps.item ||
      this.props.isMatched !== nextProps.isMatched ||
      this.props.isDisabled !== nextProps.isDisabled
      ? true
      : false;
  }

  renderSquare() {
    if (this.props.item) {
      return (
        <div
          className={
            "flex-vr-center square-" +
            (this.props.isMatched ? "matched" : "uncovered")
          }
        >
          {this.renderItem(this.props.itemType, this.props.item)}
        </div>
      );
    } else {
      if (this.props.isDisabled) {
        return <div className="square-disabled"></div>;
      }
      return (
        <div
          onClick={this.props.onClick}
          className="square-covered clickable"
        ></div>
      );
    }
  }

  renderItem(itemType, item) {
    if (itemType === ItemType.text)
      return <div className="square-inner text-center">{item}</div>;
    if (itemType === ItemType.image)
      return (
        <img className="square-inner-img" src={require(`${item.val}`)} alt="" />
      );
  }

  render() {
    return <div className="square noselect">{this.renderSquare()}</div>;
  }
}

class Board extends React.Component {
  isSquareMatched(seq) {
    return this.props.matchedSquares.includes(seq);
  }
  shouldShowItem(seq) {
    return this.isSquareMatched(seq) || this.props.activeSquares.includes(seq);
  }

  renderSquare(ind) {
    return (
      <Square
        key={ind}
        onClick={() => this.props.onItemClick(ind)}
        isMatched={this.isSquareMatched(ind)}
        isDisabled={this.props.items.map === null}
        item={this.shouldShowItem(ind) ? this.props.items.map[ind] : null}
        itemType={this.props.items.type}
      />
    );
  }

  getSquareClass() {
    const dim = this.props.dimension;
    if (dim === 2) return "col-6 col-md-6";
    if (dim === 4) return "col-3 col-md-3";
    if (dim === 6) return "col-2 col-md-2";
    if (dim === 8) return "col-1 col-md-1";
  }

  getBoardClass() {
    const dim = this.props.dimension;
    if (dim === 2) return "col-6 col-md-2";
    if (dim === 4) return "col-md-3 col-sm-12 col-12";
    if (dim === 6) return "col-md-4";
    if (dim === 8) return "col-md-6";
  }

  responsiveRender() {
    if (this.props.dimension === 8) {
      return <div className="col-2 no-padding"></div>;
    }
  }

  render() {
    const rowItemSeq = [...Array(this.props.dimension).keys()];
    return (
      <div className={`${this.getBoardClass()}`}>
        {rowItemSeq.map(row => (
          <div key={row} className="row no-padding">
            {this.responsiveRender()}
            {rowItemSeq.map(col => (
              <div className={`${this.getSquareClass()} no-padding`}>
                {this.renderSquare(this.props.dimension * row + col)}
              </div>
            ))}
          </div>
        ))}
      </div>
    );
  }
}

class Game extends React.Component {
  static NUM_PRE_GAME_ANIMATIONS = 10;
  defaultBoardDimension = 4;
  defaultCategory = Categories.pokemons;

  constructor(props) {
    super(props);
    this.state = {
      category: this.defaultCategory,
      boardDimension: this.defaultBoardDimension,
      status: GameStatus.idle,
      items: { map: null, type: null },
      activeSquares: [],
      matchedSquares: []
    };
    this.startNewGame = this.startNewGame.bind(this);
    this.stopGame = this.stopGame.bind(this);
  }

  isWin() {
    return this.state.status === GameStatus.win;
  }
  isGameOver() {
    return this.state.status === GameStatus.loose;
  }
  isInProgress() {
    return this.state.status === GameStatus.inProgress;
  }
  isIdle() {
    return this.state.status === GameStatus.idle;
  }

  handleSquareClick(currSquare) {
    const activeSquaresCount = this.state.activeSquares.length;
    if (activeSquaresCount < 2) {
      this.setState(prevState => {
        if (activeSquaresCount === 0) return { activeSquares: [currSquare] };
        if (activeSquaresCount === 1) {
          const prevSquare = prevState.activeSquares[0];
          const isMatch =
            this.state.items.map[prevSquare] ===
            this.state.items.map[currSquare];
          if (isMatch) {
            const itemsCount = Object.keys(this.state.items.map).length;
            // move the two squares from active to matched
            const matchedSquares = [
              ...prevState.matchedSquares,
              prevSquare,
              currSquare
            ];
            const isWin = itemsCount === matchedSquares.length;
            return {
              activeSquares: [],
              matchedSquares,
              status: isWin ? GameStatus.win : prevState.status
            };
          } else {
            // keep the active squares for a while so the user cannot interact with other squares
            setTimeout(() => this.setState({ activeSquares: [] }), 650);
            return { activeSquares: [prevSquare, currSquare] };
          }
        }
      });
    }
  }

  startNewGame() {
    const matchablesCount = Math.pow(this.state.boardDimension, 2) / 2;
    const matchables = getMatchables(this.state.category, matchablesCount);

    var times = 1;
    const itemSequence = [...Array(matchables.items.length * 2).keys()];
    const matchedSquares = [];
    // Pre-game animation
    const interval = setInterval(() => {
      this.setState({
        items: transformData({
          type: matchables.type,
          items: shuffle(matchables.items)
        }),
        activeSquares: itemSequence,
        matchedSquares
      });
      if (times++ === Game.NUM_PRE_GAME_ANIMATIONS) {
        clearInterval(interval);
        this.setState({
          activeSquares: [],
          status: GameStatus.inProgress
        });
      }
    }, 50);
  }

  stopGame() {
    this.setState(prevState => {
      return {
        status: GameStatus.loose,
        activeSquares: getObjectPropsAsSeq(prevState.items.map) // Uncover all squares
      };
    });
  }

  renderGameStatus() {
    if (this.isIdle()) {
      return null;
    }
    const result = this.isWin() ? (
      <span className="success">
        <b>{Labels.GAME_WIN}</b>
      </span>
    ) : this.isGameOver() ? (
      <span className="failure">
        <b>{Labels.GAME_OVER}</b>
      </span>
    ) : (
      ""
    );
    return (
      <div className="game-status">
        <span>{result}</span>
        <span>
          <Stopwatch shouldRun={this.isInProgress()} />s
        </span>
      </div>
    );
  }

  render() {
    return (
      <div className="inner-layout flex-vr-space-bt">
        <div className="row no-padding">
          <div className="col-md-6 col-12 no-padding">
            <GameControls
              boardDimension={this.state.boardDimension}
              category={this.state.category}
              inProgress={this.isInProgress()}
              onCategorySelect={category =>
                this.setState({ category: category })
              }
              onBoardDimSelect={dim => this.setState({ boardDimension: dim })}
              onStartGame={() => this.startNewGame()}
              onStopGame={() => this.stopGame()}
            />
          </div>
          <div className="col-12 col-md-6 no-padding text-right">
            {this.renderGameStatus()}
          </div>
        </div>
        <div className="flex-hr-center">
          <Board
            onItemClick={i => this.handleSquareClick(i)}
            onFinish={() => this.handleGameFinish()}
            matchedSquares={this.state.matchedSquares}
            activeSquares={this.state.activeSquares}
            dimension={
              this.state.items.map
                ? Math.sqrt(Object.keys(this.state.items.map).length)
                : this.defaultBoardDimension
            }
            items={this.state.items}
          />
        </div>
        <div></div> {/* for flex */}
      </div>
    );
  }
}

class GameControls extends React.Component {
  constructor(props) {
    super(props);

    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.handleBoardDimChange = this.handleBoardDimChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    return (
      nextProps.inProgress !== this.props.inProgress ||
      nextProps.category !== this.props.category ||
      nextProps.boardDimension !== this.props.boardDimension
    );
  }

  handleCategoryChange(event) {
    this.props.onCategorySelect(event.target.value);
  }

  handleBoardDimChange(event) {
    this.props.onBoardDimSelect(parseInt(event.target.value));
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.inProgress ? this.props.onStopGame() : this.props.onStartGame();
  }

  render() {
    return (
      <div className="game-controls">
        <form onSubmit={this.handleSubmit} className="form-inline">
          <input
            type="submit"
            value={this.props.inProgress ? "Stop Game" : "Start Game"}
          />
          <select
            value={this.props.category}
            onChange={this.handleCategoryChange}
            disabled={this.props.inProgress}
          >
            {Object.keys(Categories).map((category, ind) => (
              <option key={ind} value={Categories[category]}>
                {Categories[category]}
              </option>
            ))}
          </select>
          <select
            value={this.props.boardDimension}
            onChange={this.handleBoardDimChange}
            disabled={this.props.inProgress}
          >
            <option value={2}>2x2</option>
            <option value={4}>4x4</option>
            <option value={6}>6x6</option>
            <option value={8}>8x8</option>
          </select>
        </form>
      </div>
    );
  }
}

class Stopwatch extends React.Component {
  constructor(props) {
    super(props);
    this.state = { secondsCount: 0 };
  }

  componentDidMount() {
    this.start();
  }

  componentWillUnmount() {
    this.stop();
  }

  componentDidUpdate() {
    if (!this.props.shouldRun) {
      this.stop();
    } else if (this.timerID === null) {
      this.start();
      this.setState({ secondsCount: 0 });
    }
  }

  start() {
    this.timerID = setInterval(
      () =>
        this.setState(prevState => {
          return { secondsCount: prevState.secondsCount + 1 };
        }),
      100
    );
  }

  stop() {
    clearInterval(this.timerID);
    this.timerID = null;
  }

  render() {
    const seconds = (this.state.secondsCount / 10).toFixed(1);
    return <span>{seconds}</span>;
  }
}

class App extends React.Component {
  renderFooter() {
    return (
      <div className="footer">
        <div className="footer-content">
          <small>
            built with{" "}
            <a
              href="https://reactjs.org/"
              target="_blank"
              rel="noopener noreferrer"
            >
              React
            </a>
          </small>
          <small>
            <a
              href="https://gitlab.com/mkoterx/memo-game-react/wikis/home"
              target="_blank"
              rel="noopener noreferrer"
            >
              wIki
            </a>
          </small>
          <small>
            <a
              href="https://gitlab.com/mkoterx/memo-game-react"
              target="_blank"
              rel="noopener noreferrer"
            >
              repo
            </a>
          </small>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="layout">
        <Game />
        {this.renderFooter()}
      </div>
    );
  }
}

function transformData(data) {
  const map = {};
  const itemsSeq = shuffle([...Array(data.items.length * 2).keys()]);
  data.items.forEach(item => {
    map[itemsSeq.pop()] = item;
    map[itemsSeq.pop()] = item;
  });
  return { type: data.type, map: map };
}

export default App;
